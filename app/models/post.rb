class Post
  include Mongoid::Document
  extend Enumerize
  field :title, type: String
  field :body, type: String
  field :role
  enumerize :role, :in => %w[admin manager normal], :default => 'normal'

  class Entity < Grape::Entity
    root 'posts', 'post'
    expose :title
    expose :body, :as => :text
  end
end

"hello world"
